﻿'userstrict';
$(function () {

    var shoppingCart = $(document.body).on("click", "#shoppingCart", function () {
        $("#tempWindow").empty();
        $("#tempWindow").append("<div id='windowabc'></div>");
        var popUp = $("#windowabc").kendoWindow({
            width: "800px",
            height: "600px",
            title: "Thông tin giỏ Hàng",
            actions: ["Refresh", "Close"],
            content: "Html/shoppingcart.html",
            deactivate: function () {
                this.destroy();
            }
        }).data("kendoWindow");
        popUp.center();
        popUp.open();
        $('#windowabc').parent().find('.k-window-titlebar,.k-window-actions').css('backgroundColor', '#00a651');
    });

    var logInButton = $(document.body).on("click", "#logInButton", function () {
        $("#tempWindow").empty();
        $("#tempWindow").append("<div id='windowabc'></div>");
        var popUp = $("#windowabc").kendoWindow({
            width: "400px",
            height: "400px",
            title: "Đăng Nhập",
            actions: ["Refresh", "Close"],
            content: "Html/logOn.html",
            deactivate: function () {
                this.destroy();
            }
        }).data("kendoWindow");
        popUp.center();
        popUp.open();
        $('#windowabc').parent().find('.k-window-titlebar,.k-window-actions').css('backgroundColor', '#00a651');
    });

    var registerButton = $(document.body).on("click", "#registerButton", function () {
        $("#tempWindow").empty();
        $("#tempWindow").append("<div id='windowabc'></div>");
        var popUp = $("#windowabc").kendoWindow({
            width: "600px",
            height: "370px",
            title: "Đăng Ký",
            actions: ["Refresh", "Close"],
            content: "Html/registerPage.html",
            deactivate: function () {
                this.destroy();
            }
        }).data("kendoWindow");
        popUp.center();
        popUp.open();
        $('#windowabc').parent().find('.k-window-titlebar,.k-window-actions').css('backgroundColor', '#00a651');
    });

    var backToLogInButton = $(document.body).on("click", "#backToLogInButton", function () {
        var popUp = $("#windowabc").kendoWindow({}).data("kendoWindow");
        popUp.destroy();

        $("#tempWindow").empty();
        $("#tempWindow").append("<div id='windowabc'></div>");
        var popUp = $("#windowabc").kendoWindow({
            width: "400px",
            height: "400px",
            title: "Đăng Nhập",
            actions: ["Refresh", "Close"],
            content: "Html/logOn.html",
            deactivate: function () {
                this.destroy();
            }
        }).data("kendoWindow");
        popUp.center();
        popUp.open();
        $('#windowabc').parent().find('.k-window-titlebar,.k-window-actions').css('backgroundColor', '#00a651');
    });

    var backToRegisterButton = $(document.body).on("click", "#backToRegisterButton", function () {
        var popUp = $("#windowabc").kendoWindow({}).data("kendoWindow");
        popUp.destroy();
        $("#tempWindow").empty();
        $("#tempWindow").append("<div id='windowabc'></div>");
        var popUp = $("#windowabc").kendoWindow({
            width: "600px",
            height: "370px",
            title: "Đăng Ký",
            actions: ["Refresh", "Close"],
            content: "Html/registerPage.html",
            deactivate: function () {
                this.destroy();
            }
        }).data("kendoWindow");
        popUp.center();
        popUp.open();
        $('#windowabc').parent().find('.k-window-titlebar,.k-window-actions').css('backgroundColor', '#00a651');
    });
    var btnCheckOutClick = $(document.body).on("click", "#btnCheckOutClick", function () {
        //var popUp = $("#windowabc").kendoWindow({}).data("kendoWindow");
        //popUp.destroy();
        $("#tempWindow2").empty();
        $("#tempWindow2").append("<div id='windowabc2'></div>");
        var popUp = $("#windowabc2").kendoWindow({
            width: "600px",
            height: "430px",
            title: "Thông tin giao hàng",
            actions: ["Refresh", "Close"],
            content: "Html/deliveryPage.html",
            deactivate: function () {
                this.destroy();
            }
        }).data("kendoWindow");
        popUp.center();
        popUp.open();
        $('#windowabc2').parent().find('.k-window-titlebar,.k-window-actions').css('backgroundColor', '#00a651');
    });

    var btnDeliveryNow = $(document.body).on("click", "#btnDeliveryNow", function () {
        var popUp = $("#windowabc").kendoWindow({}).data("kendoWindow");
        popUp.destroy();
        var popUp2 = $("#windowabc2").kendoWindow({}).data("kendoWindow");
        popUp2.destroy();
        $("#tempWindow").empty();
        $("#tempWindow2").empty();
        $.notify("Thanh toán thành công.", "success");
    });
    var removeAllShoppingCart = $(document.body).on("click", "#removeAllShoppingCart", function () {
        $("#shoppingCartItems").empty();
        localStorage.setObject("shoppingCartAmount", 0)
        $.notify("Xóa giỏ hàng thành công.", "success");
    });

    var btnSearchHomePage = $(document.body).on("click", "#btnSearchHomePage", function () {
        $("#midpanel").empty();
        $("#midpanel").load("Html/searchProduct.html");
        $("#othersRow").empty();
    });

    var productViewDetails = $(document.body).on("click", "a[class='container']", function () {
        $("#midpanel").empty();
        $("#midpanel").load("Html/productDetails.html");
        $("#othersRow").empty();
    });

    var productViewBranch = $(document.body).on("click", "ul[class='z4']>li>a", function () {
        $("#midpanel").empty();
        $("#midpanel").load("Html/productBranchPage.html");
        $("#othersRow").empty();
    });
    

    var seemore = $(document.body).on("click", "div[class='see-more']>a", function () {
        $("#midpanel").empty();
        $("#midpanel").load("Html/productBranchPage.html");
        $("#othersRow").empty();
    });
    var actionRegister = $(document.body).on("click", "#btnRegisterClick", function () {
        var popUp = $("#windowabc").kendoWindow({}).data("kendoWindow");
        popUp.destroy();
        $("#tempWindow").empty();

    });
    var actionLogIn = $(document.body).on("click", "#btnLogInClick", function () {
        var popUp = $("#windowabc").kendoWindow({}).data("kendoWindow");
        popUp.destroy();
        $("#tempWindow").empty();
    });

    var addToCart = $(document.body).on("click", ".btn.btn-green.addProductToCart", function () {
        var currentCount = localStorage.getObject("shoppingCartAmount");
        if (currentCount == null)
            currentCount = 0;
        localStorage.setObject("shoppingCartAmount", parseInt(currentCount) + 1);
        reloadAmountShoppingCart(parseInt(currentCount) + 1);
        $.notify("Thêm vào giỏ.", "success");
    });
    var stayToBuy = $(document.body).on("click", "#stayToBuy", function () {
        var popUp = $("#windowabc").kendoWindow({}).data("kendoWindow");
        popUp.destroy();
        $("#tempWindow").empty();
    });
    var reloadAmountShoppingCart = function (val) {
        $("#shoppingCartAmount").html(val);
    }

});


