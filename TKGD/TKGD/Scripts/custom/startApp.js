﻿'userstrict';
$(function (window) {
    $(window).ready(function () {
        ready();
        Storage.prototype.setObject = function (key, value) {
            this.setItem(key, JSON.stringify(value));
        }

        Storage.prototype.getObject = function (key) {
            var value = this.getItem(key);
            return value && JSON.parse(value);
        }
        var popupNotification = $("#popupNotification").kendoNotification().data("kendoNotification");
    });
    var ready = function () {
        $("#loginArea").load("Html/logInSection.html");
        $("#sliderHomePage").load("Html/sliderHomepage.html");
        $("#listProductHomePage").load("Html/listProductHomePage.html");
    };

    var addToCart = function (id, money, amount) {
        var currentCount = localStorage.getObject("shoppingCartAmount");
        localStorage.setObject("shoppingCartAmount", parseInt(currentCount) + 1)
        reloadAmountShoppingCart(parseInt(currentCount) + 1);
    };
    var reloadAmountShoppingCart = function (val) {
        $("#shoppingCartAmount").html(val);

    }
    window.addToCart = addToCart;
    window.ready = ready;
});